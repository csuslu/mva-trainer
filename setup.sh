#!/bin/sh

# Some messages to the user
echo "************************************************************************"
echo -e "SETUP\t This is the MVA-Trainer setup script"
echo -e "SETUP\t "
echo -e "SETUP\t You can find more information at https://gitlab.cern.ch/skorn/mva-trainer"
echo ""

echo -e "SETUP\t Setting up environmental variables"

# Determine where this script is located (this is surprisingly difficult).
if [ "${BASH_SOURCE[0]}" != "" ]; then
    # This should work in bash.
    _src=${BASH_SOURCE[0]}
elif [ "${ZSH_NAME}" != "" ]; then
    # And this in zsh.
    _src=${(%):-%x}
elif [ "${1}" != "" ]; then
    # If none of the above works, we take it from the command line.
    _src="${1/setup.sh/}/setup.sh"
else
    echo -e "SETUP\t Failed to determine the MVA-Trainer base directory. Aborting ..."
    echo -e "SETUP\t Can you give the source script location as additional argument? E.g. with"
    echo -e "SETUP\t . ../foo/bar/scripts/setup.sh ../foo/bar/scripts"
    return 1
fi

echo -e "SETUP\t Determining distribution we are currently running with..."

# Run cat /etc/os-release and save the output to a variable
os_release_content=$(cat /etc/os-release)

# Extract NAME and VERSION using grep and awk
name=$(grep -oP -m 1 'NAME="\K[^"]+' <<< "$os_release_content")
version=$(grep -oP -m 1 'VERSION="\K[^"]+' <<< "$os_release_content")

# Display the results
echo -e "SETUP\t Running distribution: $name"
echo -e "SETUP\t Version of distribution: $version"

# Set up MVA-Trainer environment variables that are picked up from the code.
export MVA_TRAINER_BASE_DIR="$(cd -P "$(dirname "${_src}")" && pwd)"

echo -e "\e[32mSETUP\t Set up MVA_TRAINER_BASE_DIR in ${MVA_TRAINER_BASE_DIR}. \e[0m"

# Setting aliases
echo -e "\e[32mSETUP\t Setting up aliases. \e[0m"
echo ""

alias mvatrainer="python3 $MVA_TRAINER_BASE_DIR/python/mva-trainer.py"

# Determine whether Python3 is installed
echo -e "SETUP\t Checking Python3 version ..."
# Let's make sure we print the version...
pyv="$(python3 -V 2>&1)"
echo -e "\e[32mSETUP\t Detected Python3 version is $pyv \e[0m"
if ! hash python3; then
    echo -e '\e[31mERROR\t Python3 is not installed or can not be found.\e[0m'
    return 1
fi
# Ok, now that we now that we have Python3 let's check whether we also have the minimal version required (3.8)
ver=$((python3 -c 'import sys; print(sys.version_info[:][0]*100+sys.version_info[:][1])') 2>&1)
if [ "$ver" -lt "306" ]; then
    echo -e "\e[31mERROR\t Python3 3.6 or greater required!\e[0m"
    return 1
fi

# Determine whether requiered packages are installed. Cumbersome but necessary...
echo ""
echo "************************************************************************"
echo -e "SETUP\t Checking python packages..."
# Checking numpy
echo -e "SETUP\t Searching Numpy..."
if python3 -c 'import pkgutil; exit(not pkgutil.find_loader("numpy"))'; then
    NUMPY_VERSION="$(python3 -m pip show numpy | grep 'Version' | grep -v grep)"
    echo -e '\e[32mSETUP\t numpy found!\e[0m' ${NUMPY_VERSION}
else
    echo -e '\e[31mERROR\t Numpy not found\e[0m'
    return 1 
fi
# Checking pandas
echo -e "SETUP\t Searching pandas..."
if python3 -c 'import pkgutil; exit(not pkgutil.find_loader("pandas"))'; then
    PANDAS_VERSION="$(python3 -m pip show pandas | grep 'Version' | grep -v grep)"
    echo -e '\e[32mSETUP\t pandas found!\e[0m' ${PANDAS_VERSION}
else
    echo -e '\e[31mERROR\t pandas not found\e[0m'
    return 1 
fi
# Checking uproot
echo -e "SETUP\t Searching uproot..."
if python3 -c 'import pkgutil; exit(not pkgutil.find_loader("uproot"))'; then
    UPROOT_VERSION="$(python3 -m pip show uproot | grep 'Version' | grep -v grep)"
    echo -e '\e[32mSETUP\t uproot found!\e[0m' ${UPROOT_VERSION}
else
    echo -e '\e[31mERROR\t uproot not found\e[0m'
    return 1
fi

# Checking scikit-learn
echo -e "SETUP\t Searching scikit-learn..."
if python3 -c 'import pkgutil; exit(not pkgutil.find_loader("sklearn"))'; then
    SKLEARN_VERSION="$(python3 -m pip show scikit-learn | grep 'Version' | grep -v grep)"
    echo -e '\e[32mSETUP\t scikit-learn found!\e[0m' ${SKLEARN_VERSION}
else
    echo -e '\e[31mERROR\t scikit-learn not found\e[0m'
    return 1 
fi
# Checking PyTorch
echo -e "SETUP\t Searching PyTorch..."
if python3 -c 'import pkgutil; exit(not pkgutil.find_loader("torch"))'; then
    PYTORCH_VERSION="$(python3 -m pip show torch | grep 'Version' | grep -v grep)"
    echo -e '\e[32mSETUP\t PyTorch found!\e[0m' ${PYTORCH_VERSION}
else
    echo -e '\e[31mERROR\t PyTorch not found\e[0m'
    return 1
fi
# Checking PyTorch gemeotric (PyG)
echo -e "SETUP\t Searching PyG..."
if python3 -c 'import pkgutil; exit(not pkgutil.find_loader("torch_geometric"))'; then
    PYG_VERSION="$(python3 -m pip show torch_geometric | grep 'Version' | grep -v grep)"
    echo -e '\e[32mSETUP\t PyG found!\e[0m' ${PYG_VERSION}
else
    echo -e '\e[31mERROR\t PyG not found\e[0m'
    return 1
fi
# Checking pytables
echo -e "SETUP\t Searching tables..."
if python3 -c 'import pkgutil; exit(not pkgutil.find_loader("tables"))'; then
    PYTABLES_VERSION="$(python3 -m pip show tables | grep 'Version' | grep -v grep)"
    echo -e '\e[32mSETUP\t tables found!\e[0m' ${PYTABLES_VERSION}
else
    echo -e '\e[31mERROR\t tables not found\e[0m'
    return 1 
fi
# Checking matplotlib
echo -e "SETUP\t Searching matplotlib..."
if python3 -c 'import pkgutil; exit(not pkgutil.find_loader("matplotlib"))'; then
    MATPLOTLIB_VERSION="$(python3 -m pip show matplotlib | grep 'Version' | grep -v grep)"
    echo -e '\e[32mSETUP\t matplotlib found!\e[0m' ${MATPLOTLIB_VERSION}
else
    echo -e '\e[31mERROR\t matplotlib not found\e[0m'
    return 1 
fi
# Checking pydot
echo -e "SETUP\t Searching pydot..."
if python3 -c 'import pkgutil; exit(not pkgutil.find_loader("pydot"))'; then
    PYDOT_VERSION="$(python3 -m pip show pydot | grep 'Version' | grep -v grep)"
    echo -e '\e[32mSETUP\t pydot found!\e[0m' ${PYDOT_VERSION}
else
    echo -e '\e[31mERROR\t pydot not found\e[0m'
    return 1 
fi

echo "************************************************************************"
echo -e "SETUP\t Configuration finished!"
echo -e "SETUP\t "

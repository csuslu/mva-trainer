FROM rootproject/root:latest
RUN useradd --create-home --shell /bin/bash mva_trainer_user
WORKDIR /home/mva_trainer_user
RUN apt update && \
    apt install -y python3-pip && \
    apt install -y pylint && \
    apt install -y graphviz && \
    apt install -y git && \
    apt install -y emacs
RUN pip3 install --no-cache-dir --upgrade uproot && \
    pip3 install --no-cache-dir --upgrade pandas && \
    pip3 install --no-cache-dir --upgrade scikit-learn && \
    pip3 install --no-cache-dir --upgrade tables && \
    pip3 install --no-cache-dir --upgrade pydot && \
    pip3 install --no-cache-dir --upgrade skl2onnx && \
    pip3 install --no-cache-dir torch torchvision torchaudio && \
    pip3 install --no-cache-dir torch-scatter torch-sparse torch-cluster torch-spline-conv torch-geometric

USER mva_trainer_user
COPY . .
CMD ["bash"]


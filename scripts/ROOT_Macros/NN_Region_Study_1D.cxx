void NN_Region_Study_1D(){
  /* Define Parameters */
  std::string treename        = "nominal_Loose";
  std::string NtuplesLocation = "/home/steffen/Documents/My_PhD/ttZ/Ntuples/ttZ_3L_v2_full_syst_small_MVA_2/";
  std::string FakeSum         = "(fake_1lep+fake_2lep+fake_3lep)==0";
  std::string NotFakeSum      = "(fake_1lep+fake_2lep+fake_3lep)!=0";
  std::string Selection       = "(tight_1lep && tight_2lep && tight_3lep && pT_1lep>27 && pT_2lep>10 && pT_3lep>10 && (mz1<101.1876 && mz1>81.1876) && nEl+nMu==3 && min_mll>10 && nBJets85>=1&&nJets>=3)";
  std::string MCWeight        = "(XSecWeight*weight_mc*weight_pileup*weight_leptonSF*weight_bTagSF_DL1r_Continuous*weight_jvt*weight_year*138965.2)";
  std::string OutputFname     = "CutsCalculated.txt";
  std::vector<std::string> Classifier{"ttZ_binary_Classifier_v1"}; // Classifier names to be read from ntuple file

  int Nbins = 100;
  float binwidth = 1.0/Nbins;
  
  /* Outputfile */
  ofstream outfile;
  outfile.open (OutputFname.c_str(), ios::trunc);
  outfile <<"Classifier" << "\t" << "ttZ Cut" << "\t" << "ttZ_SR" << "\t" << "ttW_SR" << "\t"  << "ttH_SR" << "\t" << "WZb_SR" <<  "\t" << "WZc_SR" << "\t" << "WZl_SR" << "\t" << "ZZb_SR" << "\t" << "ZZc_SR" << "\t" << "ZZl_SR" << "\t"  << "tZ_SR" << "\t" << "tWZ_SR" << "\t" << "Fakes_SR" << "\t" << "Other_SR" << std::endl;
   
  TChain ttZChain(treename.c_str());
  TChain ttWChain(treename.c_str());
  TChain ttHChain(treename.c_str());
  TChain WZChain(treename.c_str());
  TChain ZZChain(treename.c_str());
  TChain tZChain(treename.c_str());
  TChain tWZChain(treename.c_str());
  TChain FakesChain(treename.c_str());
  TChain OtherChain(treename.c_str());
  
  //Signal Samples
  std::string ttZ410156 = NtuplesLocation+"ttZ/exported*410156*";
  std::string ttZ410157 = NtuplesLocation+"ttZ/exported*410157*";
  std::string ttZ410218 = NtuplesLocation+"ttZ/exported*410218*";
  std::string ttZ410219 = NtuplesLocation+"ttZ/exported*410219*";
  std::string ttZ410220 = NtuplesLocation+"ttZ/exported*410220*";

  //ttW
  std::string ttW410155 = NtuplesLocation+"ttX/exported*410155*";

  //ttH
  std::string ttH34644 = NtuplesLocation+"ttX/exported*34644*";

  //WZ
  std::string WZ364253 = NtuplesLocation+"diboson/exported*364253*";
  std::string WZ364284 = NtuplesLocation+"diboson/exported*364284*";

  //ZZ
  std::string ZZ345705 = NtuplesLocation+"diboson/exported*345705*";
  std::string ZZ345706 = NtuplesLocation+"diboson/exported*345706*";
  std::string ZZ364250 = NtuplesLocation+"diboson/exported*364250*";
  std::string ZZ364288 = NtuplesLocation+"diboson/exported*364288*";

  //tZ
  std::string tZ412063 = NtuplesLocation+"tZ/exported*412063*";

  //tWZ
  std::string tWZ412118 = NtuplesLocation+"tWZ/exported*412118*";

  //Fakes_2b3j
  std::string Fakes410472 = NtuplesLocation+"fakes/exported*410472*";
  std::string Fakes3661   = NtuplesLocation+"fakes/exported*3661*";
  std::string Fakes3641   = NtuplesLocation+"fakes/exported*3641*";

  //Other
  std::string Other304014 = NtuplesLocation+"Other/exported*304014*";
  std::string Other342284 = NtuplesLocation+"Other/exported*342284*";
  std::string Other342285 = NtuplesLocation+"Other/exported*342285*";
  std::string Other363358 = NtuplesLocation+"Other/exported*363358*";
  std::string Other363359 = NtuplesLocation+"Other/exported*363359*";
  std::string Other363360 = NtuplesLocation+"Other/exported*363360*";
  std::string Other36424  = NtuplesLocation+"Other/exported*36424*";
  std::string Other410080 = NtuplesLocation+"Other/exported*410080*";
  std::string Other410081 = NtuplesLocation+"Other/exported*410081*";
  
  //Filling the Chains

  //ttZ
  ttZChain.Add(ttZ410156.c_str());
  ttZChain.Add(ttZ410157.c_str());
  ttZChain.Add(ttZ410218.c_str());
  ttZChain.Add(ttZ410219.c_str());
  ttZChain.Add(ttZ410220.c_str());

  //ttW
  ttWChain.Add(ttW410155.c_str());

  //ttH
  ttHChain.Add(ttH34644.c_str());

  //WZb
  WZChain.Add(WZ364253.c_str());
  WZChain.Add(WZ364284.c_str());

  //ZZ
  ZZChain.Add(ZZ345705.c_str());
  ZZChain.Add(ZZ345706.c_str());
  ZZChain.Add(ZZ364250.c_str());
  ZZChain.Add(ZZ364288.c_str());

  //tZ
  tZChain.Add(tZ412063.c_str());

  //tWZ
  tWZChain.Add(tWZ412118.c_str());

  //Fakes
  FakesChain.Add(Fakes410472.c_str());
  FakesChain.Add(Fakes3661.c_str());
  FakesChain.Add(Fakes3641.c_str());

  //Other
  OtherChain.Add(Other304014.c_str());
  OtherChain.Add(Other342284.c_str());
  OtherChain.Add(Other342285.c_str());
  OtherChain.Add(Other363358.c_str());
  OtherChain.Add(Other363359.c_str());
  OtherChain.Add(Other363360.c_str());
  OtherChain.Add(Other36424.c_str());
  OtherChain.Add(Other410080.c_str());
  OtherChain.Add(Other410081.c_str());

  TCanvas *C1 = new TCanvas("c1","c1",600,800);
  for(int i=0; i<Classifier.size(); i++)
    {
      // std::cout << "Processing " << DibosonCut << "/" << tZCut << " ..." <<std::endl;
      double BkgYields_SR = 0;
      double SigYields = 0;
      double WZbYields = 0;
      double tWZYields = 0;
      double tZYields = 0;
      double FakeYields = 0;
      double tZ_CR_BkgYields = 0;
      double WZ_CR_BkgYields = 0;
      double tZ_CR_SigYields = 0;
      double WZ_CR_SigYields = 0;
      std::string Selection = "(tight_1lep && tight_2lep && tight_3lep && pT_1lep>27 && pT_2lep>10 && pT_3lep>10 && (mz1<101.1876 && mz1>81.1876) && nEl+nMu==3 && min_mll>10 && nBJets85>=1&&nJets>=3)";
      // Signal
      TH1F *ttZ_SR = new TH1F("ttZ","ttZ", Nbins,0,1);
      ttZChain.Draw("ttZ_binary_Classifier_v1>>ttZ",(MCWeight+"*("+Selection+"&&"+FakeSum+")").c_str());
      
      // Background
      TH1F *ttW_SR = new TH1F("ttW","ttW", Nbins,0,1);
      ttWChain.Draw("ttZ_binary_Classifier_v1>>ttW",(MCWeight+"*("+Selection+"&&"+FakeSum+")").c_str());
      
      TH1F *ttH_SR = new TH1F("ttH","ttH", Nbins,0,1);
      ttHChain.Draw("ttZ_binary_Classifier_v1>>ttH",(MCWeight+"*("+Selection+"&&"+FakeSum+")").c_str());
      
      TH1F *WZb_SR = new TH1F("WZb","WZb", Nbins,0,1);
      WZChain.Draw("ttZ_binary_Classifier_v1>>WZb",(MCWeight+"*("+Selection+"&&"+FakeSum+"&& nTruthBjets>0)").c_str());
      
      TH1F *WZc_SR = new TH1F("WZc","WZc", Nbins,0,1);
      WZChain.Draw("ttZ_binary_Classifier_v1>>WZc",(MCWeight+"*("+Selection+"&&"+FakeSum+"&& nTruthBjets==0 && nTruthCjets>0)").c_str());
      
      TH1F *WZl_SR = new TH1F("WZl","WZl", Nbins,0,1);
      WZChain.Draw("ttZ_binary_Classifier_v1>>WZl",(MCWeight+"*("+Selection+"&&"+FakeSum+"&& nTruthBjets==0 && nTruthCjets==0)").c_str());
      
      TH1F *ZZb_SR = new TH1F("ZZb","ZZb", Nbins,0,1);
      ZZChain.Draw("ttZ_binary_Classifier_v1>>ZZb",(MCWeight+"*("+Selection+"&&"+FakeSum+"&& nTruthBjets>0)").c_str());
      
      TH1F *ZZc_SR = new TH1F("ZZc","ZZc", Nbins,0,1);
      ZZChain.Draw("ttZ_binary_Classifier_v1>>ZZc",(MCWeight+"*("+Selection+"&&"+FakeSum+"&& nTruthBjets==0 && nTruthCjets>0)").c_str());
      
      TH1F *ZZl_SR = new TH1F("ZZl","ZZl", Nbins,0,1);
      ZZChain.Draw("ttZ_binary_Classifier_v1>>ZZl",(MCWeight+"*("+Selection+"&&"+FakeSum+"&& nTruthBjets==0 && nTruthCjets==0)").c_str());
      
      TH1F *tZ_SR = new TH1F("tZ","tz", Nbins,0,1);
      tZChain.Draw("ttZ_binary_Classifier_v1>>tZ",(MCWeight+"*("+Selection+"&&"+FakeSum+")").c_str());
      
      TH1F *tWZ_SR = new TH1F("tWZ","tWZ", Nbins,0,1);
      tWZChain.Draw("ttZ_binary_Classifier_v1>>tWZ",(MCWeight+"*("+Selection+"&&"+FakeSum+")").c_str());
      
      TH1F *Fakes_SR = new TH1F("Fakes","Fakes", Nbins,0,1);
      FakesChain.Draw("ttZ_binary_Classifier_v1>>Fakes",(MCWeight+"*("+Selection+"&&"+NotFakeSum+")").c_str());
      
      TH1F *Other_SR = new TH1F("Other","Other", Nbins,0,1);
      OtherChain.Draw("ttZ_binary_Classifier_v1>>Other",(MCWeight+"*("+Selection+"&&"+FakeSum+")").c_str());

      for(int j = 0; j<Nbins;j++){
	outfile << Classifier[i] << "\t"<< j*binwidth << "\t" << ttZ_SR->Integral(j,Nbins) << "\t" << ttH_SR->Integral(j,Nbins) << "\t" << ttW_SR->Integral(j,Nbins) << "\t" << WZb_SR->Integral(j,Nbins) << "\t" << WZc_SR->Integral(j,Nbins) << "\t" << WZl_SR->Integral(j,Nbins) << "\t" << ZZb_SR->Integral(j,Nbins) << "\t" << ZZc_SR->Integral(j,Nbins) << "\t" << ZZl_SR->Integral(j,Nbins) << "\t" << tZ_SR->Integral(j,Nbins) << "\t" << tWZ_SR->Integral(j,Nbins) << "\t" << Fakes_SR->Integral(j,Nbins) << "\t" << Other_SR->Integral(j,Nbins) << "\n";
      }

      /* Clean up */
      delete ttZ_SR;
      delete ttH_SR;
      delete ttW_SR;
      delete WZb_SR;
      delete WZc_SR;
      delete WZl_SR;
      delete ZZb_SR;
      delete ZZc_SR;
      delete ZZl_SR;
      delete tZ_SR;
      delete tWZ_SR;
      delete Fakes_SR;
      delete Other_SR;
    }
  outfile.close();
  delete C1;
  gApplication->Terminate();
}

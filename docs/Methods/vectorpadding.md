# Padding vectors

Often vector-like variables such as transverse momenta do not have a constant size.
Some events might e.g. have 4 jets while others have 3 or 5.
In this case the resulting vectors have different sizes.
In MVA-trainer you can assign padding values via the `PaddingValue` option to each `VARIABLE`.
These values will be filled into otherwise "empty" positions of the vector.
This way you can also use variables that are not filled for each event.

The image below describes the procedure of padding vectors using an integer vector as an example which holds either ones or zeroes.
The padding value here is -1.

![kfolding](../img/VectorPadding.png)
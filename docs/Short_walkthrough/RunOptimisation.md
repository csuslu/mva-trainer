### Optimising models using Hyperparameter Optimisation (HO)

Hyperparameter optimization is a crucial step in the process of training machine learning models. It involves finding the best set of hyperparameters, which are parameters that define the behavior and performance of a model, but are not learned directly from the data. By systematically exploring different combinations of hyperparameters, and evaluating the model's performance on a validation set, hyperparameter optimization helps to improve the model's accuracy and generalization ability.

### Performing Hyperparameter Optimisation (HO)

The HO in MVA-trainer relies on producing various altered config files based on a given seed config file. The optimisation code will then take the seed config and create copies of it where individual model parameters are changed.

To run the optimisation step you need to run:

```bash
python3 python/mva-trainer -c <config_path> --optimise --optimisationpath <path_to_directory_to_story_new_configs> --nModels <nModels>
```
where

* `<path_to_directory_to_story_new_configs>` defines a directory used to store all new config files
* `<nModels>` defines the number of new configs you want to create

Below is a list of parameters you can define in your seed config that will have an effect on the randomly generated configs:

| **Option** | **Effect** |
| ------ | ------ |






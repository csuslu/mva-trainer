# Setting up mva-trainer and Prerequisities

To obtain the code simply clone the repository using a protocol of your choice:
```sh
git clone ssh://git@gitlab.cern.ch:7999/skorn/mva-trainer.git
```
If this is your first time cloning the repository you need to verify that you have the necessary python modules installed.
For this purpose run the `setup.sh` script via:
```sh
source setup.sh
```
The script will check whether the necessary packages exist. If packages are missing an error message will be printed.
Please install any missing packages.

Each time you start a new shell you need to run the setup script again. It will set environmental variables that are picked up by the code.
The code itself is python based and hence does not need any compilation.

Depending on your system of choice different methods to run the code might be advisable.
You may
* manually install the packages and run the software. This makes sense when you are working on a "local" device
* run the software within a virtual environment. This makes sense when you run on (e.g.) lxplus or your local cluster
* use a docker container. This makes sense when you have docker available and don't want to touch anything in the code.
# Singularity usage for lxplus

From experience, setting up consistent python virtual environments on `lxplus` is not necessarily straightforward. At the same time, `docker` is not available there either. As an alternative, using `singularity` to create images can be used. After building the docker image, the following steps have to be taken for this:
```
# Setting Cache-directory outside /afs for disk quota reasons, i.e.
export SINGULARITY_CACHEDIR=/tmp/$(whoami)/singularity
mkdir $SINGULARITY_CACHEDIR

# Converting docker image into singularity image. This has to be run only once per image.
singularity build my_singularity_image.sif my_image

# Starting shell session inside singularity (with mounts to /afs, /eos, /cvmfs)
singularity shell -B /afs -B /eos -B /cvmfs my_singularity_image.sif
```
You can obtain `my_image` from the gitlab webpage of the directory via `Packages and Registries`. Make sure to copy the latest link and prepend it with `docker://` when executing the build command.
In the ensuing singularity environment, the training workflow can then be carried out. At the end, singularity can be closed again with `exit`.

## Running scripts via HTCondor on lxplus
Sometimes it might be desirable to run things using HTCondor. The scripts in `scripts/HTCondorScripts` were tested on lxplus and should work there, making your life a little easier.

The aim of these scripts is to simply allow for the `Converter.py`, `Trainer.py` and `Evaluate.py` scripts to be send to HTCondor.
To make this work you have to set up a few things.

All of this should be run outside any containerised environment. Don't forget to source the setup script. We need the environmental variables it defines.

Afterwards open `scripts/HTCondorScripts/MVA.sub` using your favourite editor.
In `scripts/HTCondorScripts/MVA.sub` you can define the structure of your HTCondor job.
The most important line for you is:
```
arguments               = $ENV(MVA_TRAINER_BASE_DIR)/Singularity-Image.sif Converter $ENV(MVA_TRAINER_BASE_DIR)/config/ExampleConfig.cfg
```
The first argument is the absolute path to your singularity image, the second one is a string (it can be `Converter`, `Trainer`, `Evaulater` or `All`, depending on what script you want to run) and the last part is the absolute path to your config file.
The directory of the job as you know it from the repo will later be created inside the main directory of the repo.
After this type
```
condor_submit scripts/HTCondorScripts/MVA.sub
```
from the main directory to submit your job to the HTCondor queue.


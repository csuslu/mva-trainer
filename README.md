# mva-trainer

This package provides a framework for the training of deep neural networks and decision trees.
Here are a few important references and links:
  - [mva-trainer website](https://mva-trainer-docs-site.docs.cern.ch/) provides in-depth information on the code and how to use it
  - Make sure you read the setup part of the documentation, e.g. [advices](https://mva-trainer-docs-site.docs.cern.ch/Containers/Docker/) on how to run the framework using containers.
  - Feel free to join the [mva-trainer mattermost channel](https://mattermost.web.cern.ch/signup_user_complete/?id=xfe8zxo5dt8eubpu3w3jgyw1cy)

Contributions to the package are welcome. Feel free to fork this project and contribute via merge requests.

### First-time setup of the code

After cloning the repository the folder
structure should ideally be the following:

```
MVA-Trainer/
    ├── config/
    ├── docs/
    ├── python/
    ├── scripts/
    └── [other files]
```

* [config](config) will host your config files.
* [python](python) hosts the main Python3 script as well as all necessary modules.
* [docs](docs) hosts the web-based documentation.
* [scripts](scripts) hosts some scripts (e.g.) for HTCondor, ROOT macros and CI-related scripts.

If you are working on a local system the following command takes care of the setup:

```sh
source setup.sh
```
The setup script checks whether all required libraries are installed. In case you are missing any libraries please install them manually.
For this you can use in example `pip3`.
Check out `requirement.txt` and install the required modules.